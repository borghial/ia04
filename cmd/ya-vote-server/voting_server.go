package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"slices"
	"sync"
	"time"

	"gitlab.utc.fr/borghial/ia04/comsoc"
)

type VotingServer struct {
	sync.Mutex
	Ballots       map[string]ballot
	BallotCounter int
}

type ballot struct {
	Rule      string               `json:"rule"`
	Deadline  time.Time            `json:"deadline"`
	VoterIds  []string             `json:"voter-ids"`
	AltsCount int                  `json:"#alts"`
	TieBreak  []comsoc.Alternative `json:"tie-break"`
	profile   map[string][]comsoc.Alternative
}

func newVotingServer() VotingServer {
	return VotingServer{
		Ballots: make(map[string]ballot),
	}
}

func (srv *VotingServer) start() {
	mux := http.NewServeMux()
	mux.HandleFunc("POST /new_ballot", srv.newBallot)
	mux.HandleFunc("POST /vote", srv.addVote)
	mux.HandleFunc("POST /result", srv.result)
	s := &http.Server{
		Addr:           "localhost:8080",
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Println("Listening on", "localhost:8080")
	log.Fatal(s.ListenAndServe())
}

type newBallotRes struct {
	BallotId string `json:"ballot-id"`
}

func (srv *VotingServer) newBallot(w http.ResponseWriter, r *http.Request) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	var newBallot ballot
	err := json.Unmarshal(buf.Bytes(), &newBallot)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	if newBallot.AltsCount <= 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "#alts<=0")
		return
	}
	if newBallot.AltsCount != len(newBallot.TieBreak) {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "#alts!=len(tie-break)")
		return
	}
	if newBallot.Deadline.Before(time.Now()) {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "Deadline dépassée")
		return
	}
	_, implemented := comsoc.Rules[newBallot.Rule]
	if !implemented {
		w.WriteHeader(http.StatusNotImplemented)
		fmt.Fprint(w, "Règle non supportée")
		return
	}
	srv.Lock()
	defer srv.Unlock()
	srv.BallotCounter++
	ballotId := fmt.Sprintf("scrutin%d", srv.BallotCounter)
	newBallot.profile = make(map[string][]comsoc.Alternative)
	srv.Ballots[ballotId] = newBallot
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	ballotRes := newBallotRes{BallotId: ballotId}
	res, _ := json.Marshal(ballotRes)
	w.Write(res)
}

type addVoteReq struct {
	AgentId  string               `json:"agent-id"`
	BallotId string               `json:"ballot-id"`
	Prefs    []comsoc.Alternative `json:"prefs"`
	Options  []int                `json:"options"`
}

func (srv *VotingServer) addVote(w http.ResponseWriter, r *http.Request) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	var newVote addVoteReq
	err := json.Unmarshal(buf.Bytes(), &newVote)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	b, ok := srv.Ballots[newVote.BallotId]
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, "Scrutin inexistant")
		return
	}
	_, exists := b.profile[newVote.AgentId]
	if exists {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprint(w, "Vote déjà effectué")
		return
	}
	if b.Deadline.Before(time.Now()) {
		w.WriteHeader(http.StatusServiceUnavailable)
		fmt.Fprint(w, "Deadline dépassée")
		return
	}
	if !slices.Contains(b.VoterIds, newVote.AgentId) {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprint(w, "Agent non autorisé")
		return
	}
	srv.Lock()
	defer srv.Unlock()
	b.profile[newVote.AgentId] = newVote.Prefs
	w.WriteHeader(http.StatusOK)
}

type resultReq struct {
	BallotId string `json:"ballot-id"`
}

type resultRes struct {
	Winner  comsoc.Alternative   `json:"winner"`
	Ranking []comsoc.Alternative `json:"ranking"`
}

func (srv *VotingServer) result(w http.ResponseWriter, r *http.Request) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	var body resultReq
	err := json.Unmarshal(buf.Bytes(), &body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	b, ok := srv.Ballots[body.BallotId]
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, "Scrutin inexistant")
		return
	}
	if b.Deadline.After(time.Now()) {
		w.WriteHeader(http.StatusTooEarly)
		fmt.Fprint(w, "Trop tôt")
		return
	}
	rule, implemented := comsoc.Rules[b.Rule]
	if !implemented {
		w.WriteHeader(http.StatusNotImplemented)
		fmt.Fprint(w, "Règle de vote inconnue")
		return
	}
	tb := comsoc.TieBreakFactory(b.TieBreak)
	tbSWF := comsoc.SWFFactory(rule.SWF, tb)
	tbSCF := comsoc.SCFFactory(rule.SCF, tb)
	var p comsoc.Profile
	for _, prefs := range b.profile {
		p = append(p, prefs)
	}
	var res resultRes
	res.Winner, err = tbSCF(p)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err.Error())
		return
	}
	res.Ranking, err = tbSWF(p)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err.Error())
		return
	}
	resBody, _ := json.Marshal(res)
	w.Write(resBody)
}
