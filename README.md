# IA04 - Activité serveur de vote

- Yasmine Hedhiri
- Alexandre Borghi

Pour démarrer le serveur en ayant $GOBIN dans le path :

```sh
go install gitlab.utc.fr/borghial/ia04/cmd/ya-vote-server@v0.1.0
ya-vote-server
```

Ou, cloner le repo et lancer :

```sh
go run ./cmd/ya-vote-server
```

L'API de bureau de vote est maintenant disponible sur `localhost:8080`. Les
règles de vote implémentées sont `majority` et `borda`.
