package comsoc

func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {
	err = checkProfileAlternative(p, p[0])
	if err != nil {
		return
	}
	alts := p[0]
	var winner Alternative = -1
	for i := 0; i < len(alts); i++ {
		altI := alts[i]
		for j := i + 1; j < len(alts); j++ {
			altJ := alts[j]
			countI := 0
			countJ := 0
			for _, pref := range p {
				for _, alt := range pref {
					if alt == altI {
						countI += 1
					} else if alt == altJ {
						countJ += 1
					}
				}
			}
			if winner == -1 && countI > countJ {
				winner = altI
			} else if winner == -1 && countJ > countI {
				winner = altJ
			} else if countI > countJ && winner == altI {
			} else if countJ > countI && winner == altJ {
			} else {
				bestAlts = []Alternative{}
				return
			}
		}
	}
	bestAlts = []Alternative{winner}
	return
}
