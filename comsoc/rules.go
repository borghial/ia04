package comsoc

type Rule struct {
	SWF func(p Profile) (Count, error)
	SCF func(p Profile) ([]Alternative, error)
}

var Rules = map[string]Rule{
	"majority": {MajoritySWF, MajoritySCF},
	"borda":    {BordaSWF, BordaSCF},
}
