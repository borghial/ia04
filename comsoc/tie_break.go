package comsoc

import "fmt"

// Dumb tie-break: returns the first alternative
func TieBreak(alts []Alternative) (Alternative, error) {
	if len(alts) == 0 {
		return 0, fmt.Errorf("TieBreak error: alts is empty")
	}
	return alts[0], nil
}

func TieBreakFactory(orderedAlts []Alternative) func([]Alternative) (Alternative, error) {
	return func(alts []Alternative) (Alternative, error) {
		for _, orderedAlt := range orderedAlts {
			for _, alt := range alts {
				if orderedAlt == alt {
					return alt, nil
				}
			}
		}
		return 0, fmt.Errorf("TieBreakFactory error: no best alt found")
	}
}

func SWFFactory(
	swf func(Profile) (Count, error),
	tb func([]Alternative) (Alternative, error),
) func(Profile) ([]Alternative, error) {
	return func(p Profile) (orderedAlts []Alternative, err error) {
		count, err := swf(p)
		if err != nil {
			return
		}
		// Add missing alternatives to map so they are counted when doing
		// len(count)
		for _, alt := range p[0] {
			count[alt] = count[alt]
		}
		for len(count) > 0 {
			bestAlt, err := tb(maxCount(count))
			if err != nil {
				return nil, err
			}
			orderedAlts = append(orderedAlts, bestAlt)
			delete(count, bestAlt)
		}
		return
	}
}

func SCFFactory(
	scf func(Profile) ([]Alternative, error),
	tb func([]Alternative) (Alternative, error),
) func(Profile) (Alternative, error) {
	return func(p Profile) (Alternative, error) {
		bestAlts, err := scf(p)
		if err != nil {
			return 0, err
		}
		alt, err := tb(bestAlts)
		return alt, err
	}
}
