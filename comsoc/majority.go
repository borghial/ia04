package comsoc

func MajoritySWF(p Profile) (count Count, err error) {
	err = checkProfileAlternative(p, p[0])
	if err != nil {
		return
	}
	count = make(Count)
	for _, pref := range p {
		count[pref[0]] += 1
	}
	return
}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p)
	bestAlts = maxCount(count)
	return
}
