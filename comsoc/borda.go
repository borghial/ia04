package comsoc

func BordaSWF(p Profile) (count Count, err error) {
	err = checkProfileAlternative(p, p[0])
	if err != nil {
		return
	}
	count = make(Count)
	for _, pref := range p {
		for rank, alt := range pref {
			count[alt] += len(pref) - 1 - rank
		}
	}
	return
}

func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := BordaSWF(p)
	bestAlts = maxCount(count)
	return
}
