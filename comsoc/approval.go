package comsoc

func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {
	err = checkProfileAlternative(p, p[0])
	if err != nil {
		return
	}
	count = make(Count)
	for agent, pref := range p {
		for i := 0; i < thresholds[agent]; i++ {
			count[pref[i]] += 1
		}
	}
	return
}

func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	count, err := ApprovalSWF(p, thresholds)
	bestAlts = maxCount(count)
	return
}
