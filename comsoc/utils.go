package comsoc

import "fmt"

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

// renvoie l'indice ou se trouve alt dans prefs
func rank(alt Alternative, prefs []Alternative) int {
	for i, a := range prefs {
		if a == alt {
			return i
		}
	}
	return -1
}

// renvoie vrai ssi alt1 est préférée à alt2
func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	return rank(alt1, prefs) > rank(alt2, prefs)
}

// renvoie les meilleures alternatives pour un décomtpe donné
func maxCount(count Count) (bestAlts []Alternative) {
	for alt, c := range count {
		if len(bestAlts) == 0 {
			bestAlts = append(bestAlts, alt)
		} else if c > count[bestAlts[0]] {
			bestAlts = []Alternative{alt}
		} else if c == count[bestAlts[0]] {
			bestAlts = append(bestAlts, alt)
		}
	}
	return
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque
// alternative de alts apparaît exactement une fois par préférences
func checkProfileAlternative(prefs Profile, alts []Alternative) error {
	altCounts := make(map[Alternative]int)
	for agent, pref := range prefs {
		for _, alt := range alts {
			altCounts[alt] = 0
		}
		for _, alt := range pref {
			altCounts[alt] = altCounts[alt] + 1
		}
		for alt, count := range altCounts {
			if count == 0 {
				return fmt.Errorf("missing alternative %d for agent %d", alt, agent)
			}
			if count > 1 {
				return fmt.Errorf("duplicate alternative %d for agent %d", alt, agent)
			}
		}
	}
	return nil
}
